##
```
Description of the task:
Currency rate bot
As an input parameter, the user must send a string of the form: 100 USD
As a response, the bot should send the equivalent in hryvnia
The exchange rate can be taken from the private API https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5 

Описание задания:
Бот курса валют
В качестве входящего параметра юзер должен прислать строку вида: 100 USD
В качестве ответа бот должен прислать эквивалент в гривне
Курс валют можно взять с API привата https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5

```

## Up the services
docker-compose up -d

## Go to the container
docker exec -it hw19adv1_app_1 bash

## Run inside the container
./ngrok http 80

## Put in your web-browser's address field:
https://api.telegram.org/bot5082871970:AAEeJeViEG25yHmygoupuDLZ2gBHJayO-gg/setWebhook?url=

## Find and copy result of ngrok program. You need URL includes this:
https://someinfo.ngrok.io

## Add to your web-browser's address field URL string you have copied. Result will be like this:
https://api.telegram.org/bot5082871970:AAEeJeViEG25yHmygoupuDLZ2gBHJayO-gg/setWebhook?url=https://someinfo.ngrok.io

## Visit URL you have made

## Go to your telegram application, find bot by:
@hillel_bot20211124_bot

## Now you can use this bot. For example send to bot meaning and type of currency, like this:
100 USD

## To exit leave the container and down services
* Pres ctrl+d in the container
* Write in the terminal: docker-compose down