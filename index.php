<?php

$sendMessageUrl = 'https://api.telegram.org/bot5082871970:AAEeJeViEG25yHmygoupuDLZ2gBHJayO-gg/sendMessage';
$courseUAHinfo = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

$telegramResponse = json_decode(file_get_contents('php://input'), true);

$bankResponse = json_decode(file_get_contents($courseUAHinfo), true);

$messageId = $telegramResponse['message']['message_id'];
$chatId = $telegramResponse['message']['chat']['id'];
$receivedText = $telegramResponse['message']['text'] ?? null;

$receivedText = explode(' ', $receivedText);

foreach ($bankResponse as $currency) {
    if ($currency['ccy'] == end($receivedText)) {
        $answerText = ($currency['buy'] * reset($receivedText)) . ' UAH';
        break;
    }
}

$response = [
    'chat_id' => $chatId,
    'text' => $answerText,
    'reply_to_message_id' => $messageId
];

if (isset($messageId)) {
    file_get_contents($sendMessageUrl . '?' . http_build_query($response));
}
